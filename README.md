# README

## About
rexbacklight is a program designed to operate similarly to xbacklight. There is even a configure option to enable xbacklight arguments to make this a drop in replacement. However unlike xbacklight, this program does not use X11 to control the backlight devices. rexbacklight directly interacts with sysfs to change backlight levels, read their current status, and find their max output level. This has the advantage of allowing rexbacklight to run without the X server running and will work even if X doesn't recognize your backlight device, which was my original inspiration to make this project.

I've also since added a program to control LED devices, rexledctl (best name I could think of). This one was inspired by the fact that my laptop's keyboard backlight wasn't controllable by any program I had and the hardware button didn't work either. So now I have my own program which can control it. It was so similar in function to rexbacklight that I decided to just add some compile time `#define`'s and make a few things more modular in the code and have the same source files make 2 different programs.

Either program can be built alone or both together. This is configured using some cmake magic.

Current version is 1.5.2 as of writing this, so if the version is much newer, remind me to update this readme.

```
rexbacklight version v1.5.2

Usage: rexbacklight [argument] [options] [argument]

Options:
--device|-d
    select which device to control
--fade|-f
    change brightness over time interval
--steps|-s
    number of steps over which to fade
--get|-g
    print current brightness level to stdout
--list|-l
    print device names to stdout and exit
--restore|-R
    reassign previously saved device values
--no-save|-N
    do not write any data to the restore file
--help|-h
    print this help message and exit
--version
    print program version and exit

Arguments:
  =<percentage>
  -<percentage>
  +<percentage>
  off
  max
  min

rexbacklight Copyright (C) 2018-2021 rexy712
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions; see the GNU GPLv3 for details.
A copy of the GPLv3 is available with the source in the file 'LICENSE'
```

## Dependencies
Requires the rjp ([rexy's json parser][rjp]) library for reading restore file, though this can be disabled via cmake configuration.  
Either python3 or sh compatible shell to run some build scripts.

## Building
##### Run the following commands
```
mkdir build
cd build
ccmake ..
#or just 'cmake ..' and specify options on command line
make
```

## Installing
##### Run the following commands from the build directory after building
```
sudo make install
```
##### Reload the udev rules for them to take effect, otherwise just reboot
```
sudo udevadm control --reload-rules
sudo udevadm trigger
```

## Uninstalling
##### Run the following commands from the build directory
```
sudo make uninstall
```

[rjp]: https://gitlab.com/rexy712/rjp
