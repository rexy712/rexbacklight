#!/bin/sh

output="$(git describe --always --dirty --abbrev --match="NeVeRmAtCh")"

if test -n output;then
	echo "#define GIT_COMMIT_HASH \"$output\""
	if grep -q "dirty" <<< "$output";then
		echo "#define GIT_DIRTY"
	fi
fi
