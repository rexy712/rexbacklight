#!/bin/sh

output="$(git rev-parse --abbrev-ref HEAD)"

if test -n "$output";then
	echo "#define GIT_BRANCH_NAME \"$output\" //test"
fi
