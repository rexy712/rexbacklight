#!/usr/bin/python

import subprocess

output = subprocess.run(['git', 'tag', "--points-at", "HEAD"], stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()

if output:
	if output[0] == 'v' or output[0] == 'V':
		print("#define GIT_TAG_NAME " + '"' + output[1:] + '"')
