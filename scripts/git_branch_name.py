#!/usr/bin/python

import subprocess

output = subprocess.run(['git', 'rev-parse', "--abbrev-ref", "HEAD"], stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()

if output:
	print("#define GIT_BRANCH_NAME " + '"' + output + '"')
