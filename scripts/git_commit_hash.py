#!/usr/bin/python

import subprocess

output = subprocess.run(['git', 'describe', "--always", "--dirty", "--abbrev", "--match=\"NeVeRmAtCh\""], stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()

if output:
	print("#define GIT_COMMIT_HASH " + '"' + output + '"')
	if output.find("dirty") != -1:
		print("#define GIT_DIRTY")
