#!/bin/sh

output="$(git tag --points-at HEAD)"

if test -n "$output";then
	if test "${output:0:1}" = "v" || test "${output:0:1}" = "V";then
		echo "#define GIT_TAG_NAME \"${output:1}\""
	fi
fi
