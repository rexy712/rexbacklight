/**
	rexbacklight
	Copyright (C) 2018-2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RESTORE_H
#define RESTORE_H

#include <rjp.h>

#include "common.h"
#include "cmd.h"

void save_restore_file(struct string_array* devices, struct arg_values* args);
int restore_to_delta(struct arg_values* curr);
void prep_restore(struct arg_values* a);
RJP_value* find_matching_json_device(const char* name, RJP_value* root);
RJP_value* read_restore_file(const char* file);

#endif
