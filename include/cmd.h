/**
	rexbacklight
	Copyright (C) 2018-2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXBACKLIGHT_CMD_H
#define REXBACKLIGHT_CMD_H

#define OP_INC     1
#define OP_DEC     2
#define OP_SET     4
#define OP_LIST    8
#define OP_RESTORE 16
#define OP_GET     128
#define OP_NONE    0
#define OP_VERSION 254
#define OP_USAGE   255

#define ARG_FLAG_NONE    0
#define ARG_FLAG_NO_SAVE 1

struct cmd_arg{
	const char* lopt;
	const char* sopt;
#ifdef XBACKLIGHT_COMPAT_OPTIONS
	const char* xopt;
#endif
	const char* desc;
};

extern struct cmd_arg rexbacklight_args[];
extern int rexbacklight_args_length;

struct arg_values{
	struct arg_values* next;

	//Specific device to control
	//NULL means all devices
	const char* device;

	//starting brightness
	int act_start;
	//value to write in backlight file
	int act_delta;
	//output percentage
	float delta;

	//How many seconds to transition
	int fade_duration;

	int fade_steps;

	union{
		int operation;
		int num_values;
	};
	int flags;
};



void free_cmd_args(struct arg_values* a);
struct arg_values process_cmd_args(int argc, char** argv);
int process_op(struct arg_values* arg, float min, float current, float max);

#endif
