/**
	rexbacklight
	Copyright (C) 2018-2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program, If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REXBACKLIGHT_COMMON_H
#define REXBACKLIGHT_COMMON_H

#ifdef REXBACKLIGHT_DEBUG
#define DEBUG_PRINT(s, ...) printf(s, __VA_ARGS__)
#else
#define DEBUG_PRINT(s, ...)
#endif

#define RETVAL_INVALID_FILE         1
#define RETVAL_INVALID_DIR          2
#define RETVAL_UNRECOGNIZED_OPTION  3
#define RETVAL_MISSING_OPTION       4
#define RETVAL_INVALID_DEVICE       5
#define RETVAL_MEM_ERROR            6
#define RETVAL_INTERNAL_ERROR       7
#define RETVAL_SUCCESS              EXIT_SUCCESS

#define IO_ERROR_DIR   "directory"
#define IO_ERROR_FILE  "file"
#define IO_ERROR_OPEN  "open"
#define IO_ERROR_READ  "read"
#define IO_ERROR_WRITE "write to"

extern int return_value;

struct string_array{
	char** list;
	int size;
};
void free_string_array(struct string_array* s);

void io_error(const char* error, const char* type, const char* name);
void io_error_2(const char* error, const char* type, const char* name, const char* name2);
void io_error_3(const char* error, const char* type, const char* name, const char* name2, const char* name3);
void mem_error(void);

_Noreturn void version(void);
_Noreturn void usage(int exit_val);

#endif
