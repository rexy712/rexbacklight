/**
	rexbacklight
	Copyright (C) 2018-2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h> //printf
#include <string.h> //strcmp, strlen
#include <rjp.h>
#include <sys/types.h> //getpwnam
#include <pwd.h> //getpwnam
#include <unistd.h> //chdir
#include <stdlib.h> //malloc, free

#include "restore.h"
#include "common.h"
#include "cmd.h"
#include "globals.h"
#include "rexbacklight.h"

static char* get_home_folder(void){
	char* user = getenv("SUDO_USER");
	char* home = getenv("HOME");
	if(!home || user){
		if(!user){
			return NULL;
		}
		struct passwd* pw_entry = getpwnam(user);
		if(!pw_entry){
			return NULL;
		}
		home = pw_entry->pw_dir;
	}
	return home;
}
static char* restore_file(void){
	char* home_folder = get_home_folder();
	if(!home_folder)
		return NULL;

	//"/.config/rexbacklight.json"
	size_t home_folder_len = strlen(home_folder);
	size_t conf_len = strlen(restore_file_suffix());
	char* rest_file = malloc(home_folder_len + conf_len + 1);
	strncpy(rest_file, home_folder, home_folder_len);
	strncpy(rest_file+home_folder_len, restore_file_suffix(), conf_len);
	rest_file[home_folder_len+conf_len] = 0;
	return rest_file;
}

RJP_value* read_restore_file(const char* file){
	size_t filesize;
	char* file_contents;
	int i;
	FILE* fp = fopen(file, "r");
	if(!fp){
		return NULL;
	}

	fseek(fp, 0, SEEK_END);
	filesize = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	file_contents = malloc(filesize+1);
	i = fread(file_contents, filesize, 1, fp);
	fclose(fp);
	file_contents[filesize] = 0;
	RJP_value* root = rjp_parse(file_contents, RJP_PARSE_ALLOW_COMMENTS, NULL);
	free(file_contents);
	return root;
}

static RJP_value* restore_file_handle(void){
	static RJP_value* rf = NULL;
	if(!rf){
		char* f = restore_file();
		rf = read_restore_file(f);
		free(f);
	}
	return rf;
}

RJP_value* find_matching_json_device(const char* name, RJP_value* root){
	RJP_object_iterator it;
	rjp_init_object_iterator(&it, root);
	for(RJP_value* curr = rjp_object_iterator_current(&it);curr;curr = rjp_object_iterator_next(&it)){
		if(!strcmp(rjp_member_key(curr)->value, name)){
			return curr;
		}
	}
	return NULL;
}

//convert a restoration operation to a set operation
int restore_to_delta(struct arg_values* curr){
	RJP_value* root = restore_file_handle();
	if(!root){
		fprintf(stderr, "Could not restore! No restore file found!\n");
		return 0;
	}
	RJP_value* match = find_matching_json_device(curr->device, root);
	if(!match){
		fprintf(stderr, "No matching device '%s' in restore file!\n", curr->device);
		return 0;
	}
	curr->operation = OP_SET;
	curr->delta = rjp_get_float(match);
	return 1;
}
void prep_restore(struct arg_values* a){
	for(struct arg_values* curr = a;curr;curr = curr->next){
		restore_to_delta(curr);
	}
}

void save_restore_file(struct string_array* devices, struct arg_values* args){
  RJP_object_iterator it;
	RJP_value* rf = restore_file_handle();
	if(!rf)
		rf = rjp_new_object();
  rjp_init_object_iterator(&it, rf);
	for(RJP_value* mem = rjp_object_iterator_current(&it);mem;mem = rjp_object_iterator_next(&it)){
		for(struct arg_values* curr = args->next, *prev = args;curr;prev = curr, curr = curr->next){
			if(curr->operation != OP_SET || curr->flags & ARG_FLAG_NO_SAVE){
				prev->next = curr->next;
				free(curr);
				curr = prev;
				continue;
			}
			if(!strcmp(rjp_member_key(mem)->value, curr->device)){
        rjp_set_float(mem, curr->delta);
				prev->next = curr->next;
				free(curr);
				break;
			}
		}
	}
	for(struct arg_values* curr = args->next;curr;curr = curr->next){
		if(curr->operation == OP_SET){
			RJP_value* newmem = rjp_new_member(rf, curr->device, 0);
      rjp_set_float(newmem, curr->delta);
    }
	}
	free_cmd_args(args);

	char* tmp = rjp_to_json(rf, RJP_FORMAT_NONE);
	char* rfil = restore_file();
	FILE* restf = fopen(rfil, "w");
	if(!restf){
		io_error(IO_ERROR_OPEN, IO_ERROR_FILE, rfil);
	}else{
		fprintf(restf, "//File generated by %s\n%s\n", executable_name(), tmp);
		fclose(restf);
	}
	free(rfil);
	rjp_free(tmp);
	rjp_free_value(rf);
}



