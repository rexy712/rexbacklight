/**
	rexbacklight
	Copyright (C) 2018-2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h> //fprintf
#include <stdlib.h> //malloc, free
#include <string.h> //strlen, strcpy
#include <unistd.h> //chdir
#include <sys/types.h> //opendir
#include <dirent.h> //opendir
#include <sys/time.h> //gettimeofday
#include <time.h> //nanosleep

#include "common.h"
#include "cmd.h"
#include "globals.h"
#include "config.h"

#ifdef ENABLE_RESTORE_FILE
#include "restore.h"
#endif

/////////////////////////////////////////////////////////////////////////////////////////////


#ifdef REXBACKLIGHT

//This is where backlight devices can be found in sysfs
const char* device_dir(void){return "/sys/class/backlight/";}
//name of the program being run so that we print the correct name in the usage
const char* executable_name(void){return "rexbacklight";}
//location of the restore file in the home directory
const char* restore_file_suffix(void){return "/.config/rexbacklight.json";}

#elif defined(REXLEDCTL)

//This is where led devices can be found in sysfs
const char* device_dir(void){return "/sys/class/leds/";}
const char* executable_name(void){return "rexledctl";}
const char* restore_file_suffix(void){return "/.config/rexledctl.json";}

#else
#error "UNDEFINED PROGRAM NAME!"
#endif

const char* brightness_file(void){return "brightness";}
const char* max_brightness_file(void){return "max_brightness";}


/////////////////////////////////////////////////////////////////////////////////////////////

//create a copy of a string and append a '/' character to the end
char* add_slash_to(const char* name, size_t namelen){
	char* newname = malloc(namelen + 2);
	memcpy(newname, name, namelen);
	newname[namelen++] = '/';
	newname[namelen] = 0;
	return newname;
}

//Get a list of led/backlight devices in sysfs
struct string_array get_device_sources(void){
	DIR* fd;
	struct dirent* dir;
	struct string_array arr = {0};
	int list_size = 8;

	fd = opendir(device_dir());
	if(!fd){
		io_error(IO_ERROR_OPEN, IO_ERROR_DIR, device_dir());
		return_value = RETVAL_INVALID_DIR;
		return arr;
	}

	arr.list = malloc(sizeof(const char*) * list_size);
	if(!arr.list){
		closedir(fd);
		mem_error();
		return_value = RETVAL_MEM_ERROR;
		return arr;
	}

	for(arr.size = 0;(dir = readdir(fd));){
		int slen;
		//ignore '.' and '..'
		if(!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, "..")){
			continue;
		}

		//Resize list if more than 8 led/backlight devices exist (not likely)
		if(arr.size >= list_size){
			char** tmp = malloc(sizeof(const char*) * (list_size += 8));
			if(!tmp){
				mem_error();
				return_value = RETVAL_MEM_ERROR;
				free_string_array(&arr);
				closedir(fd);
				return (struct string_array){0};
			}
			for(int j = 0;j < arr.size;++j){
				tmp[j] = arr.list[j];
			}
			free(arr.list);
			arr.list = tmp;
		}

		slen = strlen(dir->d_name);
		arr.list[arr.size] = malloc(slen + 1);
		if(!arr.list[arr.size]){
			mem_error();
			return_value = RETVAL_MEM_ERROR;
			free_string_array(&arr);
			closedir(fd);
			return (struct string_array){0};
		}
		strncpy(arr.list[arr.size], dir->d_name, slen);
		arr.list[arr.size][slen] = 0;
		++arr.size;
	}

	closedir(fd);
	return arr;
}


//Read a float from a file
float get_brightness(const char* file){
	char buff[127];
	FILE* bright = fopen(file, "r");
	if(!bright){
		io_error(IO_ERROR_OPEN, IO_ERROR_FILE, file);
		return_value = RETVAL_INVALID_FILE;
		return 0;
	}
	if(!fgets(buff, sizeof(buff), bright)){
		io_error(IO_ERROR_READ, IO_ERROR_FILE, file);
		return_value = RETVAL_INVALID_FILE;
		fclose(bright);
		return 0;
	}
	fclose(bright);
	return atof(buff);
}

//return milliseconds since last boot
double get_time(void){
	struct timespec t;
	clock_gettime(CLOCK_BOOTTIME, &t);
	return ((t.tv_sec * 1000.0) + (t.tv_nsec * 0.0000001));
}
//sleep thread for milliseconds
void sleep_for(double time){
	DEBUG_PRINT("sleeping for %lf milliseconds\n", time);
	struct timespec ts;
	ts.tv_sec = (long)(time*0.0001);
	ts.tv_nsec = ((time) - ts.tv_sec*1000) * 1000*1000;
	nanosleep(&ts, NULL);
}

void return_to_root_dir(void){
	if(chdir(device_dir())){
		io_error(IO_ERROR_OPEN, IO_ERROR_DIR, device_dir());
		return_value = RETVAL_INVALID_DIR;
	}
}
//update brightness incrementally over requested millisecond time interval
void fade_out(struct arg_values* arg){
	FILE* fd;
	double start, end;
	double tdelta = 0;       //amount of time that has passed in ms
	float value = arg->act_start;        //current value to write to file
	double step_delta = (double)arg->fade_duration / arg->fade_steps;
	double step_inc = step_delta;
	float brdelta = (float)(arg->act_delta - arg->act_start) / arg->fade_steps; //amount brightness needs to change per iteration

	while(arg->fade_duration > tdelta){
		//write
		start = get_time();
		fd = fopen(brightness_file(), "w+");
		if(!fd){
			char* devname = add_slash_to(arg->device, strlen(arg->device));
			io_error_3(IO_ERROR_OPEN, IO_ERROR_FILE, device_dir(), devname, brightness_file());
			return_value = RETVAL_INVALID_FILE;
			free(devname);
			return_to_root_dir();
			return;
		}
		fprintf(fd, "%d", (int)value);
		fclose(fd);
		end = get_time();

		//calc time delta
		tdelta += end - start;

		//calc next value to write
		value += brdelta;

		//waste excess time until next step
		if(step_delta > tdelta){
			sleep_for(step_delta - tdelta);
			tdelta = step_delta;
		}
		//calc end of next step
		step_delta += step_inc;
	}
	fd = fopen(brightness_file(), "w+");
	if(!fd){
		char* devname = add_slash_to(arg->device, strlen(arg->device));
		io_error_3(IO_ERROR_OPEN, IO_ERROR_FILE, device_dir(), devname, brightness_file());
		return_value = RETVAL_INVALID_FILE;
		free(devname);
		return_to_root_dir();
		return;
	}
	fprintf(fd, "%d", arg->act_delta);

	fclose(fd);
	return_to_root_dir();
}

int prep_offset(struct arg_values* args){
	if(chdir(args->device)){
		io_error_2(IO_ERROR_OPEN, IO_ERROR_DIR, device_dir(), args->device);
		return RETVAL_INVALID_DIR;
	}
	args->act_start = get_brightness(brightness_file());
	float act_max = get_brightness(max_brightness_file());
	args->act_delta = process_op(args, 0, args->act_start, act_max);
	float start_pec = (args->act_start / act_max) * 100.0;
	if(args->operation == OP_INC){
		args->delta = start_pec + args->delta;
	}else if(args->operation == OP_DEC){
		args->delta = start_pec - args->delta;
	}
	if(args->delta > 100)
		args->delta = 100;
	else if(args->delta < 0)
		args->delta = 0;
	args->operation = OP_SET;
	//chdir back in do_assignment
	return 0;
}

//Run get operation
int do_get(struct arg_values* args){
	float curr, max;
	if(chdir(args->device)){
		io_error_2(IO_ERROR_OPEN, IO_ERROR_DIR, device_dir(), args->device);
		return RETVAL_INVALID_DIR;
	}
	curr = get_brightness(brightness_file());
	max = get_brightness(max_brightness_file());
	if(return_value == RETVAL_INVALID_FILE && (!curr || !max)){
		fprintf(stdout, "%s", args->device);
	}else{
		fprintf(stdout, "%-25s %.2f\n", args->device, curr / max * 100);
	}
	if(chdir(device_dir())){
		io_error(IO_ERROR_OPEN, IO_ERROR_DIR, device_dir());
		return RETVAL_INVALID_DIR;
	}
	return return_value;
}
//Run list operation
void do_list(struct arg_values* args){
	printf("%s\n", args->device);
}

//Run requested operation on a per device basis
int run_device_operations(struct arg_values* a){
	int changes = 0;
	for(a = a->next;a;a = a->next){
		switch(a->operation){
		case OP_LIST:
			do_list(a);
			break;
		case OP_GET:
			return_value = do_get(a);
			break;
#ifdef ENABLE_RESTORE_FILE
		case OP_RESTORE:
			if(!restore_to_delta(a)){
				continue;
			}
#endif
		case OP_INC:
		case OP_DEC:
		case OP_SET:
			if(prep_offset(a))
				break;
			++changes;
			fade_out(a);
			break;
		};
	}
	return changes;
}

//If a global operation is requested, run this to fill arg with all devices, each with the global operation
void populate_args(struct arg_values* arg, struct string_array* devices){
	struct arg_values* curr = arg;
	for(size_t i = 0;i < devices->size;++i){
		curr->next = malloc(sizeof(struct arg_values));
		curr->next->device = devices->list[i];
		curr->next->delta = arg->delta;
		curr->next->fade_duration = arg->fade_duration;
		curr->next->fade_steps = arg->fade_steps;
		curr->next->operation = arg->operation;
		curr->next->flags = arg->flags;
		curr = curr->next;
	}
	curr->next = NULL;
	arg->num_values = devices->size;
}


int main(int argc, char** argv){

	struct arg_values args; //A linked list of devices and the requested settings.
	struct string_array device_names; //List of all led/backlight devices in sysfs.
	int should_save = 0;

	args = process_cmd_args(argc, argv);
	if(args.operation == OP_USAGE){
		usage(return_value);
	}else if(args.operation == OP_VERSION){
		version();
	}

	device_names = get_device_sources();

	//If there are no led/backlights, we can't do anything
	if(device_names.size == 0){
#ifdef REXBACKLIGHT
		fprintf(stderr, "No backlight devices found!\n");
#elif defined(REXLEDCTL)
		fprintf(stderr, "No led devices found!\n");
#endif
		free_string_array(&device_names);
		free_cmd_args(&args);
		return RETVAL_INVALID_DEVICE;
	}

	//Make sure all explicit devices actually exist
	for(struct arg_values* curr = args.next;curr;curr = curr->next){
		for(int i = 0;i < device_names.size;++i){
			if(!strcmp(curr->device, device_names.list[i])){
				goto continue_outer;
			}
		}
		fprintf(stderr, "No such device '%s'\n", curr->device);
		free_string_array(&device_names);
		free_cmd_args(&args);
		return RETVAL_INVALID_DEVICE;
		continue_outer:;
	}

	//Change to the base directory for all sysfs leds/backlights
	if(chdir(device_dir())){
		io_error(IO_ERROR_READ, IO_ERROR_DIR, device_dir());
		free_string_array(&device_names);
		free_cmd_args(&args);
		return RETVAL_INVALID_DIR;
	}

	//on a global operation, populate list with all devices
	if(!args.next){
		populate_args(&args, &device_names);
	}
	should_save = (run_device_operations(&args) > 0);
#ifdef ENABLE_RESTORE_FILE
	if(should_save)
		save_restore_file(&device_names, &args); //performs cleanup on args
	else
		free_cmd_args(&args);
#else
	free_cmd_args(&args);
#endif

	free_string_array(&device_names);
	return return_value;
}
