/**
	rexbacklight
	Copyright (C) 2018-2021 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"
#include "common.h"
#include "cmd.h"
#include "globals.h"

#include <stdio.h> //printf
#include <stdlib.h> //exit

int return_value = RETVAL_SUCCESS;

void io_error(const char* error, const char* type, const char* name){
	fprintf(stderr, "Unable to %s %s '%s'!\n", error, type, name);
}
void io_error_2(const char* error, const char* type, const char* name, const char* name2){
	fprintf(stderr, "Unable to %s %s '%s%s'!\n", error, type, name, name2);
}
void io_error_3(const char* error, const char* type, const char* name, const char* name2, const char* name3){
	fprintf(stderr, "Unable to %s %s '%s%s%s'!\n", error, type, name, name2, name3);
}
void mem_error(void){
	fprintf(stderr, "Failed to allocate memory! Unable to continue!\n");
}

//Clean up a string array
void free_string_array(struct string_array* s){
	int i;
	for(i = 0;i < s->size;++i)
		free(s->list[i]);
	free(s->list);
}

_Noreturn void version(void){
	printf("%s version %s\n", executable_name(), REXBACKLIGHT_VERSION);
	exit(return_value);
}

_Noreturn void usage(int exit_val){
	int i;
	printf("%s version %s\n\n", executable_name(), REXBACKLIGHT_VERSION);
	printf("Usage: %s [argument] [options] [argument]\n\n", executable_name());

	printf("Options:\n");
	for(i = 0;i < rexbacklight_args_length;++i){
		int printed = 0;
		if(rexbacklight_args[i].lopt){
			printf("%s", rexbacklight_args[i].lopt);
			printed = 1;
		}
		if(rexbacklight_args[i].sopt){
			if(printed)
				printf("|");
			else
				printed = 1;
			printf("%s", rexbacklight_args[i].sopt);
		}
#ifdef XBACKLIGHT_COMPAT_OPTIONS
		if(rexbacklight_args[i].xopt){
			if(printed)
				printf("|");
			else
				printed = 1;
			printf("%s", rexbacklight_args[i].xopt);
		}
#endif //XBACKLIGHT_COMPAT_OPTIONS
		if(printed){
			printf("\n");
			printf("    %s\n", rexbacklight_args[i].desc);
		}
	}
	printf("\n");
	printf("Arguments:\n");
	printf("  =<percentage>\n");
	printf("  -<percentage>\n");
	printf("  +<percentage>\n");
	printf("  off\n");
	printf("  max\n");
	printf("  min\n");

	printf("\n%s Copyright (C) 2018-2021 rexy712\n", executable_name());
	printf("This program comes with ABSOLUTELY NO WARRANTY.\n");
	printf("This is free software, and you are welcome to redistribute it\n");
	printf("under certain conditions; see the GNU GPLv3 for details.\n");
	printf("A copy of the GPLv3 is available with the source in the file 'LICENSE'\n");

	exit(exit_val);
}
